# textile-ci-docker-image
# ARCHIVED as [textile is dead](https://github.com/textileio/textile/commits/master/)

Docker image to push to textile bucket in CI

Inspired by Textile's [GitHub action](https://github.com/textileio/github-action-buckets)

## Usage

Needed variables (which I defined as secret variables):

- `TEXTILE_API_KEY`
- `TEXTILE_API_SECRET`
- `TEXTILE_THREAD`

Pipeline config:

```yaml
build:
  stage: build
  ...
  artifacts:
    paths:
      - build/

deploy to textile:
  stage: publish
  dependencies:
    - build
  image: registry.gitlab.com/txlab/textile-ci-docker-image/master
  variables:
    TEXTILE_BUCKET: fe-dev
    TEXTILE_PUSH_DIR: ./build
  script:
    - /scripts/push.js
```


## Alternative: [hub-cli image](https://hub.docker.com/r/tennox/textile-hub-cli)

```yaml
deploy:textile:
  stage: publish
  interruptible: true
  dependencies:
    - build
  image:
    name: tennox/textile-hub-cli
    entrypoint: [""] # needed for GitLab CI 
  variables:
    HUB_BUCKET: frontend-dev
  script:
    - 'hub buck existing' # for debugging
    - "BUCKET_KEYS=$(hub buck existing | grep fe-dev | awk '{print $3}')"
    - "BUCKETS_MATCHING=$(echo $BUCKET_KEYS | wc -w)"
    - 'echo $BUCKET_KEYS $BUCKETS_MATCHING'
    - 'if [ $BUCKETS_MATCHING -ne 1 ]; then echo "Matching bucket count: $BUCKETS_MATCHING"; /bin/false; fi'
    - 'cd build/'
    - 'hub buck push --thread=$HUB_THREAD --key=$BUCKET_KEYS -y'
```
